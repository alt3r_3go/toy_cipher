#!/usr/bin/env python3

from Crypto.Random import random as cr

def gen_sbox(bitlen):
    '''Generate SBOX values for bitlen x bitlen SBOX'''
    sbox = {}

    # Just three principles we use for our SBOX:
    # 1) It's bijective (S(x) != S'(x) for any x != x')
    # 2) No fixed point (S(x) != x for any x in F2/4)
    # Substitution values are generated randomly
    for i in range(2**bitlen):
        while True:
            tmp_rnd = cr.getrandbits(bitlen)
            if tmp_rnd != i and tmp_rnd not in sbox.values():
                sbox[i] = tmp_rnd
                print("Added {} to SBOX at position {}".format(tmp_rnd, i))
                break

    print("Final SBOX: {}".format(sbox))

def main():
    gen_sbox(4)

if __name__ == '__main__':
    main()