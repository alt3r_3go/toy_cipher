#!/usr/bin/env python3

from Crypto.Random import random as cr

def gen_shiftrow_coeffs(number_of_coeffs):
    '''Generate number_of_coeffs ShiftRow coefficients'''
    # Principles we use for our coeff set (ci):
    # 1) c0 is 0
    # 2) c0 < c1 < c2 < ... < ci
    # 3) Coeff values are integers from range (0, 15]
    # 4) Coeff values are generated randomly
    coeffs = [0]

    for i in range(number_of_coeffs - 1):
        while True:
            tmp_rnd = cr.randint(1, 15)
            if tmp_rnd not in coeffs:
                coeffs.append(tmp_rnd)
                print("Added {} to ShiftRow coeffs".format(tmp_rnd))
                break
    # We want an increasing sequence
    coeffs.sort()

    print("Final ShiftRow coeff set: {}".format(coeffs))

def main():
    gen_shiftrow_coeffs(4)

if __name__ == '__main__':
    main()