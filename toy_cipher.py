import numpy as np
import logging
import cointoss.cipher as cointoss
import cointoss.utils as utils

def main():
    '''Main execution function - encrypts/decrypts "all-zeroes" or "all-ones" case'''
    # Change to logging.DEBUG if you want all the details
    logging.basicConfig(level=logging.INFO)

    # Key, plaintext and ciphertext lists must be little-endian, i.e. the LSB is at list index 0.
    key = np.zeros(16, np.uint8)
    plaintext = np.zeros(8, np.uint8)

    # Try both "all-zeroes" and "all-ones" cases
    for value in [0x00, 0xFF]:
        key.fill(value)
        plaintext.fill(value)

        # Encrypt
        ciphertext = cointoss.encrypt(plaintext, key)
        logging.info('Plaintext {}\nencrypted into ciphertext {}'.format(utils.bytel2hexsl(plaintext),
                                                                         utils.bytel2hexsl(ciphertext)))

        # Decrypt
        plaintext2 = cointoss.decrypt(ciphertext, key)
        logging.info('Ciphertext {}\ndecrypted into plaintext {}'.format(utils.bytel2hexsl(ciphertext),
                                                                         utils.bytel2hexsl(plaintext2)))

if __name__ == "__main__":
    main()