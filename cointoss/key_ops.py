# Key operations for COINTOSS toy cipher

import numpy as np
import logging
from . import constants
from . import utils

def key_sub_columns(key_register, sbox):
    ''' Applies S-Box to the rightmost 8 columns of the key_register.
        Returns: key_register matrix with substituted columns.
    '''
    logging.debug('Key register before SBOX:\n{}'.format(key_register))
    for column in range(-1, -9, -1):
        logging.debug('Key register column {} before SBOX: {}'.format(column, key_register[:,column]))
        key_register[:,column] = utils.sub_nibble(key_register[:,column], sbox)
        logging.debug('Key register column {} after SBOX: {}'.format(column, key_register[:,column]))
    logging.debug('Key register after SBOX:\n{}'.format(key_register))

    return key_register

def key_gen_feistel_transform(key_register):
    ''' Applies 1-round generalized Feistel transformation to key_register.
        Returns: key_register with transformed rows.
    '''
    logging.debug('Key register before Feistel:\n{}'.format(key_register))
    # Save Row0 for later
    row0 = np.copy(key_register[0,:])
    # Row0' = (Row0 <<< 8) ^ Row1
    key_register[0,:] = np.roll(key_register[0,:], -8) ^ key_register[1,:]
    # Row1' = Row2
    key_register[1,:] = key_register[2,:]
    # Row2' = (Row2 <<< 16) ^ Row3
    key_register[2,:] = np.roll(key_register[2,:], -16) ^ key_register[3,:]
    # Row3' = Row0
    key_register[3,:] = row0
    logging.debug('Key register after Feistel:\n{}'.format(key_register))

    return key_register

def key_add_round_const(key_register, round_num):
    ''' Adds (bitwise XOR) round constant to the lowest 5 bits of the key_register.
        Returns: key_register with round constant added.
    '''
    # Prepare the binary form of the round const. Remove leading zeroes.
    round_const_bin = np.delete(np.unpackbits(np.array(constants.ROUND_CONSTS[round_num], dtype=np.uint8)), [0, 1, 2])
    logging.debug('{}\n{}'.format(round_num, round_const_bin))

    # Do the addition
    key_register[0,-5:] = key_register[0,-5:] ^ round_const_bin

    return key_register

def key_update_key_register(key_register, round_num):
    ''' Updates key_register by
            * Applying S-Box to the rightmost 8 columns;
            * Applying 1-round generalized Feistel transformation to rows;
            * XORing round constant to the first 5 bits of the key register;
        Returns: key_register after update.
    '''
    logging.debug('Key register before SBOX:\n{}'.format(key_register))
    key_register = key_sub_columns(key_register, constants.SBOX_ENC)
    logging.debug('Key register after SBOX:\n{}'.format(key_register))
    logging.debug('Key register before Feistel:\n{}'.format(key_register))
    key_register = key_gen_feistel_transform(key_register)
    logging.debug('Key register after Feistel:\n{}'.format(key_register))
    logging.debug('Key register before adding round const:\n{}'.format(key_register))
    key_register = key_add_round_const(key_register, round_num)
    logging.debug('Key register after adding round const:\n{}'.format(key_register))

    return key_register

def key_gen_round_keys(key):
    ''' Generate an array of subkeys.
        Precondition: seed key is a list of 16 bytes.
        Returns: array of NUM_ROUNDS+1 binary arrays of 4*16 bits.
    '''
    # Array of all round subkeys
    subkeys = np.empty((constants.NUM_ROUNDS + 1, 4, 16), np.uint8)

    logging.debug('Seed key bytes: {}'.format(key))
    # Prepare the key register by converting list of 16 bytes
    # into 4*32 numpy array of bits
    key_reg = np.fliplr(np.array(key,dtype=np.uint8).reshape((4,4)))
    key_reg_bin = np.unpackbits(key_reg, axis=1)
    logging.debug('Seed key matrix:\n{}'.format(key_reg_bin))

    # Generate round subkeys
    for round_num in range(constants.NUM_ROUNDS):
        # Extract the round key
        subkeys[round_num, :, :] = key_reg_bin[:,16:]
        logging.debug('Subkey for round {}:{}\n{}'.format(round_num,
                                                          utils.bytel2hexsl(utils.binm2bytel(subkeys[round_num, :, :])),
                                                          subkeys[round_num, :, :]))

        # Update the key register
        key_reg_bin = key_update_key_register(key_reg_bin, round_num)

    # Extract the last subkey
    subkeys[-1, :, :] = key_reg_bin[:,16:]
    logging.debug('Last subkey:{}\n{}'.format(utils.bytel2hexsl(utils.binm2bytel(subkeys[-1, :, :])), subkeys[-1, :, :]))

    return subkeys
