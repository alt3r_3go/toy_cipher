# Constant definitions for COINTOSS toy cipher and RECTANGLE cipher

# Number of cipher rounds
NUM_ROUNDS = 25

# Key schedule round constants
ROUND_CONSTS = [0x01, 0x02, 0x04, 0x09, 0x12,
                0x05, 0x0B, 0x16, 0x0C, 0x19,
                0x13, 0x07, 0x0F, 0x1F, 0x1E,
                0x1C, 0x18, 0x11, 0x03, 0x06,
                0x0D, 0x1B, 0x17, 0x0E, 0x1D]

# S-Box description
# Encryption
SBOX_COINTOSS_ENC = {0x00: 0x07, 0x01: 0x05, 0x02: 0x0E, 0x03: 0x0B, 0x04: 0x02, 0x05: 0x06, 0x06: 0x04, 0x07: 0x0D,
                     0x08: 0x01, 0x09: 0x08, 0x0A: 0x09, 0x0B: 0x0C, 0x0C: 0x03, 0x0D: 0x0F, 0x0E: 0x00, 0x0F: 0x0A}
# Decryption - reverse the encryption one
SBOX_COINTOSS_DEC = {v: k for k, v in SBOX_COINTOSS_ENC.items()}
# S-Box description from the RECTANGLE cipher
# Encryption
SBOX_RECTANGLE_ENC = {0x00: 0x06, 0x01: 0x05, 0x02: 0x0C, 0x03: 0x0A, 0x04: 0x01, 0x05: 0x0E, 0x06: 0x07, 0x07: 0x09,
                      0x08: 0x0B, 0x09: 0x00, 0x0A: 0x03, 0x0B: 0x0D, 0x0C: 0x08, 0x0D: 0x0F, 0x0E: 0x04, 0x0F: 0x02}
# Decryption
SBOX_RECTANGLE_DEC = {v: k for k, v in SBOX_RECTANGLE_ENC.items()}

# ShiftRow transform description, index is row number, value is number of bits to rotate left.
# Encryption. Negatives mean "to the left" due to NumPy peculiarity.
SHIFT_ROW_COINTOSS_ENC = [0, -6, -7, -12]
# Decryption - reverse the encryption one
SHIFT_ROW_COINTOSS_DEC = [-1 * x for x in SHIFT_ROW_COINTOSS_ENC]
# ShiftRow transform description from the RECTANGLE cipher
# Encryption. Negatives mean "to the left" due to NumPy peculiarity.
SHIFT_ROW_RECTANGLE_ENC = [0, -1, -12, -13]
# Decryption
SHIFT_ROW_RECTANGLE_DEC = [-1 * x for x in SHIFT_ROW_RECTANGLE_ENC]

# Choose the S-Box - change here if you want to try RECTANGLE instead of COINTOSS
SBOX_ENC = SBOX_COINTOSS_ENC
SBOX_DEC = SBOX_COINTOSS_DEC

# Choose ShiftRow transform - change here if you want to try RECTANGLE instead of COINTOSS
SHIFT_ROW_ENC = SHIFT_ROW_COINTOSS_ENC
SHIFT_ROW_DEC = SHIFT_ROW_COINTOSS_DEC
