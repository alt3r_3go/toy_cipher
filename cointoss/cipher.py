import numpy as np
import logging
from . import constants
from . import utils
from . import key_ops

def cipher_sub_columns(cipher_state, sbox):
    ''' Applies S-Box to all columns of the cipher state.
        Returns: cipher state matrix after transformation.
    '''
    logging.debug('Cipher state before S-Box:\n{}'.format(cipher_state))
    for column in range(16):
        logging.debug('Cipher state column {} before S-Box: {}'.format(column, cipher_state[:,column]))
        cipher_state[:,column] = utils.sub_nibble(cipher_state[:,column], sbox)
        logging.debug('Cipher state column {} after S-Box: {}'.format(column, cipher_state[:,column]))
    logging.debug('Cipher state after S-Box:\n{}'.format(cipher_state))

    return cipher_state

def cipher_shift_row(cipher_state, shiftrow_def):
    ''' Shifts cipher state rows according to shiftrow_def description.
        Returns: cipher state matrix after transformation.
    '''
    logging.debug('Cipher state before ShiftRow:\n{}'.format(cipher_state))
    for row_num in range(len(shiftrow_def)):
        # Rotate the respective row in cipher state left by respective number of bits
        logging.debug('Rotating cipher state row {} left by {} bits'.format(row_num, shiftrow_def[row_num]))
        cipher_state[row_num,:] = np.roll(cipher_state[row_num,:], shiftrow_def[row_num])
    logging.debug('Cipher state after ShiftRow:\n{}'.format(cipher_state))

    return cipher_state

def encrypt(plaintext, key):
    ''' Encrypt a 128-bit block of plaintext (provided as 8-byte list) using key.
        Prerequisite: plaintext is an 8-byte list.
        Prerequisite: key is a 16-byte list.
        Returns: ciphertext as a list of 8 bytes.
    '''
    # Generate round keys
    round_keys = key_ops.key_gen_round_keys(key)

    logging.debug('Plaintext bytes: {}'.format(utils.bytel2hexsl(plaintext)))
    # Convert plaintext into 4*16 bit matrix of cipher state
    ciphertext = utils.bytel2cbinm(plaintext)
    logging.debug('Initial cipher state matrix:\n{}'.format(ciphertext))

    for round_num in range(constants.NUM_ROUNDS):
        # AddRoundKey
        ciphertext = ciphertext ^ round_keys[round_num,:,:]
        # SubColumns
        ciphertext = cipher_sub_columns(ciphertext, constants.SBOX_ENC)
        # ShiftRows
        ciphertext = cipher_shift_row(ciphertext, constants.SHIFT_ROW_ENC)
    # Last AddRoundKey - the respective subkey is the last one in the subkeys array.
    ciphertext = ciphertext ^ round_keys[-1,:,:]

    logging.debug('Cipher state matrix after full encryption cycle:\n{}'.format(ciphertext))

    # Do the reverse (binary matrix) -> (list of bytes) transformation
    ciphertext = utils.binm2bytel(ciphertext)

    return ciphertext


def decrypt(ciphertext, key):
    ''' Decrypt a 128-bit block of ciphertext (provided as 8-byte list) using key.
        Prerequisite: ciphertext is an 8-byte list.
        Prerequisite: key is a 16-byte list.
        Returns: plaintext as a list of 8 bytes.
    '''
    # Generate round keys
    round_keys = key_ops.key_gen_round_keys(key)

    logging.debug('Ciphertext bytes: {}'.format(utils.bytel2hexsl(ciphertext)))
    # Convert ciphertext into 4*16 bit matrix of cipher state
    plaintext = utils.bytel2cbinm(ciphertext)
    logging.debug('Initial cipher state matrix:\n{}'.format(plaintext))

    # Last AddRoundKey - the respective subkey is the last one in the subkeys array.
    plaintext = plaintext ^ round_keys[-1,:,:]

    for round_num in range(constants.NUM_ROUNDS - 1, -1, -1):
        # ShiftRows
        plaintext = cipher_shift_row(plaintext, constants.SHIFT_ROW_DEC)
        # SubColumns
        plaintext = cipher_sub_columns(plaintext, constants.SBOX_DEC)
        # AddRoundKey
        plaintext = plaintext ^ round_keys[round_num,:,:]

    # Do the reverse (binary matrix) -> (list of bytes) transformation
    plaintext = utils.binm2bytel(plaintext)

    return plaintext
