# Utility functions

import numpy as np

def bytel2hexsl(byte_list):
    ''' "Byte List To Hex String List" conversion.
        Converts list of bytes into list of hex-encoded string representations
        of those bytes. Used for pretty printing.
        Returns: list of hex-encoded bytes as strings.
    '''
    return [hex(x) for x in byte_list]

def binm2bytel(bin_matrix):
    ''' "Binary Matrix To Byte List" conversion.
        Converts binary matrix (cipher state or key register) into byte list.
        Precondition: binary matrix row length is a multiple of 8 bits.
        Returns: list of bytes.
    '''
    byte_list = np.fliplr(np.packbits(bin_matrix, axis=1)).flatten()

    return byte_list

def bytel2cbinm(byte_list):
    ''' "Byte List To Cipher state Binary Matrix" conversion.
        Converts plain-/ciphertext byte list into cipher state binary matrix.
        Precondition: the list is 8 bytes long.
        Precondition: byte list is arranged to have LSB at list index 0.
        Returns: binary cipher state 4*16 matrix.
    '''
    bin_matrix = np.fliplr(np.array(byte_list,dtype=np.uint8).reshape((4,2)))
    bin_matrix = np.unpackbits(bin_matrix, axis=1)

    return bin_matrix

def sub_nibble(nibble, sbox):
    ''' Substitutes 4-bit array with another 4-bit array based on sbox.
        Returns: 4-bit array.
    '''
    # Need to flip to account for bits location per algorithm vs. numpy's slice
    nibble_flipped = np.flip(nibble)
    # Convert to byte, accounting for padding
    sbox_in = np.packbits(nibble_flipped)[0] >> 4
    # Do the substitution
    sbox_out = sbox[sbox_in]
    # Convert back to the binary array, taking only last 4 digits (unpackbits does padding)
    # and flipping the order back to account for slice vs. algorithm bit locations.
    sbox_out_bin = np.flip(np.unpackbits(np.array(sbox_out, dtype=np.uint8))[-4:])

    return sbox_out_bin
