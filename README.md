Description
===========

This is a **toy** implementation of a lightweight Substitution-Permutation Network-based
cipher I called COINTOSS, heavily influenced by the so called LS cipher design approach [1]
in general and RECTANGLE cipher (as a nice example) construction [2] in particular.

Do not use it for any production tasks, this is a purely educational endeavor.

The main idea was to randomly generate (thus the name) the S-Box and ShiftRow
transformation rules to illustrate the importance of selecting them carefully and
open the door to trying various cryptanalysis techniques and step-by-step cipher
hardening in a simplified setting. Key schedule is fully reused from RECTANGLE
as I don't see any meaningful ways of randomizing it, plus it facilitates comparison
between COINTOSS and RECTANGLE's security.

While implementing COINTOSS, I've also implemented RECTANGLE, you only need
to change the selection of S-Box and ShiftRow transformations in `cointoss/constants.py`.

See `doc` directory for more details. Run `toy_cipher.py` to see it in action.

References
===========

[1] Grosso, V., Leurent, G., Standaert, F. X., & Varıcı, K. (2014, March).
    LS-designs: Bitslice encryption for efficient masked software implementations.
    In Fast Software Encryption (pp. 18-37). Springer Berlin Heidelberg.
    [pdf at inria.fr](https://hal.inria.fr/hal-01093491/document)

[2] Zhang, W., Bao, Z., Lin, D., Rijmen, V., Yang, B. & Verbauwhede, I. (2014).
    RECTANGLE: A Bit-slice Ultra-Lightweight Block Cipher Suitable for Multiple
    Platforms. Cryptology ePrint Archive, Report 2014/084, version 20140207:151850.
    [pdf at eprint.iacr.org](https://ia.cr/2014/084)